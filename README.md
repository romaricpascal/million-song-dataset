## Requirements

- [Python 3](https://www.python.org/)
- [venv module](https://docs.python.org/3/library/venv.html) for python

	<details>
		<summary>Ubuntu</summary>

	```
	sudo apt install python3-venv
	```

	</details>

- [pip](https://pypi.org/project/pip/)

	<details>
		<summary>Ubuntu</summary>

	```
	sudo apt install python3-pip
	```

	</details>

- [jupyter](https://jupyter.org/)

## Setup

1. Create and activate a virtual environment for the project.

	This will avoid its dependencies conflicting with any packages installed globally

	```sh
	python -m venv .venv
	source .venv/bin/activate
	```

	Once the virtual environment is created, you'll only need to activate it when coming back to the project

	```sh
	source .venv/bin/activate
	```

2. Install the dependencies using `pip`

	```sh
	pip install -r requirements.txt
	```